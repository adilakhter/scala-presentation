## Scala Presentation 

### How to get started. 

1. Clone this repository via `git clone`.  
2. CD to the root of the source directory, ie. `scala-presentation/` where `main.html` is located. 
3. Run a static web server: `python -m http.server`. Other ways to run the server: [https://gist.github.com/willurd/5720255](https://gist.github.com/willurd/5720255).
4. Access the presentation at the following URL:`localhost:8000/main.html`